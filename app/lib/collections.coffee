﻿import { FilesCollection } from 'meteor/ostrio:files'

@db = {}
db.users = Meteor.users
db.categories = new Mongo.Collection('Categories')
db.items = new Mongo.Collection('Items')
db.orders = new Mongo.Collection('Orders')
db.Images = new FilesCollection({collectionName: 'Images', storagePath: 'e:/app_images'})