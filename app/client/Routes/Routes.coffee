﻿BlazeLayout.setRoot('body')

#Home Routes

FlowRouter.route '/', { name: 'home',
action:(p,q) ->
  BlazeLayout.render 'front', { body:"home"} 
  }

#Checkout Routes

FlowRouter.route '/cart', { name: 'cart',
action:(p,q) ->
  if  Meteor.userId()
    BlazeLayout.render 'front', { body:"cart"} 
  else
    FlowRouter.go('/')
}

#Items Routes

FlowRouter.route '/items', { name: 'items',
action:(p,q) ->
  if  Meteor.userId()
    BlazeLayout.render 'back', { body:"items"} 
  else
    FlowRouter.go('/')
}

#Categories Routes

FlowRouter.route '/categories', { name: 'categories',
action:(p,q) ->
  if  Meteor.userId()
    BlazeLayout.render 'back', { body:"categories"} 
  else
    FlowRouter.go('/')
}

#Orders Routes

FlowRouter.route '/orders', { name: 'orders',
action:(p,q) ->
  if  Meteor.userId()
    BlazeLayout.render 'back', { body:"orders"} 
  else
    FlowRouter.go('/')
}

#Orders Report Routes

FlowRouter.route '/ordersreport', { name: 'orders',
action:(p,q) ->
  if  Meteor.userId()
    BlazeLayout.render 'back', { body:"ordersreport"} 
  else
    FlowRouter.go('/')
}

#Reports Routes

FlowRouter.route '/reports', { name: 'reports',
action:(p,q) ->
  if  Meteor.userId()
    BlazeLayout.render 'back', { body:"reports"} 
  else
    FlowRouter.go('/')
}

#Reports Login

FlowRouter.route '/login', { name: 'login',
action:(p,q) ->
  if not Meteor.userId()
    BlazeLayout.render 'front', { body:"home"}
    $('#login-modal').click()
  else
    FlowRouter.go('/')
}





