﻿Template.front.onCreated () ->
  t = Template.instance()
  t.isAdmin = new ReactiveVar(false)
  
  @subscribe 'user'

Template.front.helpers
  isAdmin: () ->
    try
      if Meteor.userId()
        user = db.users.findOne({'_id':Meteor.userId()})
        if user.profile.role == 'admin'
          return true
        else 
          return false 
  name: () ->
    try
      if Meteor.userId()
        user = db.users.findOne({'_id':Meteor.userId()}) 
        return user.profile.name 
  signedIn: () ->
    if Meteor.userId()
      return true
    else  
      return false 
  cartitems: () ->
    if Session.get('cart') != undefined 
      return Object.keys(Session.get('cart')).length
    else
      return '0'
  

Template.front.events
  'click .navbar-brand': (e,t) ->
    FlowRouter.go('/')
  'click .btn-cart': (e,t) ->
    if Meteor.userId()
     FlowRouter.go('/cart')
    else
      $('#myModal').modal('show')
    $('.navbar-toggle').click()
  'click .btn-modal': (e,t) ->
    $('#myModal').modal('show')
  'click .btn-signout': (e,t) ->
    Meteor.logout()
    FlowRouter.go('/')
    $('.navbar-toggle').click()
  'click .btn-dashboard': (e,t) ->
    FlowRouter.go('/items')
  'click #btn-addtocart':(e,t)->
    e.preventDefault()
    e.stopPropagation()
    selecteditem = Session.get('selecteditem')
    cart = Session.get('cart')
    if cart == undefined
      cart = []
    cart.push(selecteditem)
    Session.setPersistent('cart',cart)
    $('#myModal').modal('hide')
    