﻿Template.back.onCreated () ->
  Meteor.call 'role.isAdmin', (err, res) ->
    unless res
      FlowRouter.go('/')
  t = Template.instance()
  t.isAdmin = new ReactiveVar(false)
  
  @subscribe 'users'

Template.back.helpers
  isAdmin: () ->
    if Meteor.userId()
      user = db.users.findOne({'_id':Meteor.userId()})
      if user.profile.role == 'admin'
        return true
      else 
        return false 
  name: () ->
    if Meteor.userId()
      user = db.users.findOne({'_id':Meteor.userId()}) 
      return user.profile.name 
  signedIn: () ->
    if Meteor.userId()
      return true
    else  
      return false 

Template.back.events
  'click .navbar-brand': (e,t) ->
    FlowRouter.go('/items')
    $('.navbar-toggle').click()
  'click .btn-signout': (e,t) ->
    Meteor.logout()
    FlowRouter.go('/')
    $('.navbar-toggle').click()
  'click .btn-items': (e,t) ->
    FlowRouter.go('/items')
    $('.navbar-toggle').click()
  'click .btn-categories': (e,t) ->
    FlowRouter.go('/categories')
    $('.navbar-toggle').click()
  'click .btn-orders': (e,t) ->
    FlowRouter.go('/orders')
    $('.navbar-toggle').click()
  'click .btn-ordersreport': (e,t) ->
    FlowRouter.go('/ordersreport')
    $('.navbar-toggle').click()
  'click .btn-home': (e,t) ->
    FlowRouter.go('/')
    