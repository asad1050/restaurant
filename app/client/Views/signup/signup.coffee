﻿Template.signup.onCreated () -> 

Template.signup.events
  'click #btn-signup':(e,t)->
    e.preventDefault()
    e.stopPropagation()
    $("#btn-signup").prop('disabled', true)
    name = $('#txt-name-su').val()
    email = $('#txt-email-su').val()
    password = $('#txt-password-su').val()
    if name == '' or email == '' or password == ''
      Bert.alert('Please enter email and password','warning')
      return
    if not email.includes("@")
      Bert.alert('Please enter correct email','warning')
      return
    Meteor.call 'user.create', name, email, password, (err,res)->
      if err
        Bert.alert('Unable to sign up','danger')
      else
        swal({
          title: "Success"
          text: "Signed In Successfully"
          icon: "success"
        }).then(()-> $('#btn-modal-close').click())
    $("#btn-signup").prop('disabled', false)
      


  'keyup #txt-password-su': (e,t) ->
    if e.which == 13
      e.preventDefault()
      e.stopPropagation()
      $("#btn-signup").prop('disabled', true)
      name = $('#txt-name-su').val()
      email = $('#txt-email-su').val()
      password = $('#txt-password-su').val()
      if name == '' or email == '' or password == ''
        Bert.alert('Please enter email and password','warning')
        return
      if not email.includes("@")
        Bert.alert('Please enter correct email','warning')
        return
      Meteor.call 'user.create', name, email, password, (err,res)->
        if err
          Bert.alert('Unable to sign up','danger')
        else
          swal({
            title: "Success"
            text: "Signed In Successfully"
            icon: "success"
          }).then((ok)-> $('#btn-modal-close').click())
      $("#btn-signup").prop('disabled', false)
          