﻿Template.categories.onCreated () ->
  
  Session.set('imageProg', false)
  t = Template.instance()
  t.uId = new ReactiveVar(null)
  t.uimg = new ReactiveVar(null)
  

  @subscribe 'categories'
  @subscribe 'images'

Template.categories.helpers
  
  getcategories: () ->
    return db.categories.find()
  
Template.categories.events
  'click #btn-create':(e,t)->
    e.preventDefault()
    e.stopPropagation()
    if $('#txt-name').val() == '' 
      Bert.alert('Please, fill all input fields','warning')
      return
    if Session.get('imageId') == undefined
      Bert.alert('Please, choose image','warning')
      return
    name = $('#txt-name').val()
    if Session.get('imageProg') == true
      Bert.alert('Please, wait while image is uploading','warning')
      return
    if Session.get('imageId') != undefined
      url = db.Images.findOne({'_id':Session.get('imageId')}).link()
      img = url.substr(url.indexOf("/cdn/storage/"), (url.length - 1))
    $("#btn-create").prop('disabled', true)
    Meteor.call 'categories.add', name, img,  (err, res) ->
      if (err)
        swal({
          title: "Error"
          text: "Unable to create category"
          icon: "error"
        }) 
        console.log err
      else
        $('#txt-name').val('')
        $('#name').focus()
        Session.set('imageReset',true)
        Session.set('imageId',undefined)
        swal({
          title: "Success"
          text: "Category created successfully"
          icon: "success"
        }).then((ok)-> $('#myModal').modal('hide'))
    $("#btn-create").prop('disabled', false)


  'click [data-action=remove-item]': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    
    id = e.currentTarget.value
    $("[data-action=remove-item]").prop('disabled', true)
    Meteor.call 'categories.remove', id, (err, res) ->
      if (err)
        swal({
          title: "Error"
          text: "Unable to remove category"
          icon: "error"
        }) 
        console.log err
      else
        swal({
          title: "Success"
          text: "Category is successfully removed"
          icon: "success"
        })
    $("[data-action=remove-item]").prop('disabled', false)
