﻿Template.cart.onCreated () ->
  @subscribe 'user'

Template.cart.helpers
  getcart: () ->
    return Session.get('cart')
  iscart: () ->
    if Session.get('cart') != undefined 
      if Object.keys(Session.get('cart')).length == 0
        return false
      else
        return true
    else
      return false

Template.cart.events
  'click [data-action=remove-item]': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    
    id = e.currentTarget.value
    cart = Session.get('cart')
    i=0
    while (i<cart.length)
      if cart[i]["_id"] == id
        index = i
      i++
    cart.splice( index, 1 )   
    Session.setPersistent('cart',cart)
    swal({
          title: "Success"
          text: "Item removed successfully"
          icon: "success"
        })

  'click #btn-checkout': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    items = Session.get('cart')
    now = moment()
    time = new Date(now)
    name = db.users.findOne({'_id':Meteor.userId()}).profile.name
    $("#btn-checkout").prop('disabled', true)
    Meteor.call 'orders.add',items, Meteor.userId(), name, time ,  (err, res) ->
      if (err)
        swal({
          title: "Error"
          text: "Unable to checkout"
          icon: "error"
        }) 
        console.log err
      else
        Session.setPersistent('cart',[])
        swal({
          title: "Success"
          text: "Order placed successfully"
          icon: "success"
        })
    $("#btn-checkout").prop('disabled', false)
      
      