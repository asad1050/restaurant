﻿
Template.uploadForm.onCreated(function () {
    this.currentUpload = new ReactiveVar(false);
    this.completed = new ReactiveVar(false);
    this.err = new ReactiveVar(false);
    
});


Template.uploadForm.helpers({
    currentUpload() {
        Session.set('imageReset', false);
        return Template.instance().currentUpload.get();
    },
    completed() {
    return Template.instance().completed.get();
    },
    err() {
        return Template.instance().err.get();
    },
    reset() {
        Template.instance().currentUpload.set(false);
        Template.instance().completed.set(false);
        Template.instance().err.set(false);
        return Session.get('imageReset')
}
});

Template.uploadForm.events({
    'change #fileInput'(e, template) {
        template.completed.set(false);
        template.err.set(false);
        if (e.currentTarget.files && e.currentTarget.files[0]) {
            // We upload only one file, in case
            // multiple files were selected
            upload = db.Images.insert({
                file: e.currentTarget.files[0],
                streams: 'dynamic',
                chunkSize: 'dynamic'
            }, false);

            upload.on('start', function () {
                template.currentUpload.set(this);
                Session.set('imageProg', true);
            });

            upload.on('end', function (error, fileObj) {
                Session.set('imageProg', false);
                if (error) {
                    template.err.set(true);
                } else {
                    template.completed.set(true);
                    Session.set('imageId', upload.config.fileId);
                }
                template.currentUpload.set(false);
            });

            upload.start();
        }
    }
});