﻿Template.items.onCreated () ->
  
  Session.set('imageProg', false)
  t = Template.instance()
  t.uId = new ReactiveVar(null)
  t.uimg = new ReactiveVar(null)
  
  
  

  @subscribe 'items'
  @subscribe 'images'
  @subscribe 'categories'

Template.items.onRendered () ->
  $('#btn-update').hide()

Template.items.helpers
  
  getitems: () ->
    return db.items.find()
  getcategories: () ->
    return db.categories.find()
  
Template.items.events
  'click #btn-new': (e,t) ->
    $('#btn-create').show()
    $('#btn-update').hide()
    $('#txt-name').val('')
    $('#txt-price').val('')
    $('#txt-description').val('')
    $('#txt-name').focus()
    Session.set('imageReset',true)
    Session.set('imageId',undefined)
    $('#myModal').modal('show')    

  'click #btn-create':(e,t)->
    e.preventDefault()
    e.stopPropagation()
    str = db.Images.findOne({'_id':Session.get('imageId')}).link()
    console.log str
    console.log str.indexOf("/cdn/storage/")
    console.log url.substr(url.indexOf("/cdn/storage/"), (url.length - 1))
    return
    if $('#txt-name').val() == '' or $('#txt-price').val() == ''
      Bert.alert('Please, fill all input fields','warning')
      return
    if Session.get('imageId') == undefined
      Bert.alert('Please, choose image','warning')
      return
    
    name = $('#txt-name').val()
    price = $('#txt-price').val()
    description = $('#txt-description').val()
    categoryid = $('#txt-categories').val()
    categoryname = db.categories.findOne('_id':categoryid).name
    if Session.get('imageProg') == true
      Bert.alert('Please, wait while image is uploading','warning')
      return
    if Session.get('imageId') != undefined
      url = db.Images.findOne({'_id':Session.get('imageId')}).link()
      img = url.substr(url.indexOf("/cdn/storage/"), (url.length - 1))
    $("#btn-create").prop('disabled', true)
    Meteor.call 'items.add', name, img, price, description, categoryname, categoryid,  (err, res) ->
      if (err)
        swal({
          title: "Error"
          text: "Unable to create item"
          icon: "error"
        }) 
        console.log err
      else
        $('#txt-name').val('')
        $('#txt-price').val('')
        $('#txt-description').val('')
        $('#txt-name').focus()
        Session.set('imageReset',true)
        Session.set('imageId',undefined)
        swal({
          title: "Success"
          text: "Item created successfully"
          icon: "success"
        }).then((ok)-> $('#myModal').modal('hide'))
    $("#btn-create").prop('disabled', false)


  'click [data-action=remove-item]': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    
    id = e.currentTarget.value
    $("[data-action=remove-item]").prop('disabled', true)
    Meteor.call 'items.remove', id, (err, res) ->
      if (err)
        swal({
          title: "Error"
          text: "Unable to remove item"
          icon: "error"
        }) 
        console.log err
      else
        swal({
          title: "Success"
          text: "Item is successfully removed"
          icon: "success"
        })
    $("[data-action=remove-item]").prop('disabled', false)
  
  'click [data-action=edit-item]': (e,t) ->
    e.preventDefault()
    e.stopPropagation()  
    $('#txt-name').val(e.currentTarget.attributes.name.nodeValue)
    $('#txt-description').val(e.currentTarget.attributes.description.nodeValue)
    $('#txt-price').val(e.currentTarget.attributes.price.nodeValue)
    $( '#txt-categories option:selected' ).text(e.currentTarget.attributes.categoryname.nodeValue)
    $( '#txt-categories option:selected' ).val(e.currentTarget.attributes.categoryid.nodeValue)
    t.uimg.set(e.currentTarget.attributes.img.nodeValue)
    $('#btn-create').hide()
    $('#btn-update').show()
    id = e.currentTarget.value
    t.uId.set(id)
    $('#myModal').modal('show')

  'click #btn-update':(e,t)->
    e.preventDefault()
    e.stopPropagation()
    if $('#txt-name').val() == '' or $('#txt-price').val() == ''
      Bert.alert('Please, fill all input fields','warning')
      return
    if Session.get('imageId') == undefined
      Bert.alert('Please, choose image','warning')
      return
    name = $('#txt-name').val()
    price = $('#txt-price').val()
    description = $('#txt-description').val()
    categoryid = $('#txt-categories').val()
    categoryname = db.categories.findOne('_id':categoryid).name
    if Session.get('imageProg') == true
      Bert.alert('Please, wait while image is uploading','warning')
      return
    if Session.get('imageId') != undefined
      url = db.Images.findOne({'_id':Session.get('imageId')}).link()
      img = url.substr(url.indexOf("/cdn/storage/"), (url.length - 1))
    else
      img = t.uimg.get()
    $("#btn-update").prop('disabled', true)
    Meteor.call 'items.update',  t.uId.get(), name, img, price, description, categoryname, categoryid,  (err, res) ->
      if (err)
        swal({
          title: "Error"
          text: "Unable to update item"
          icon: "error"
        }) 
        console.log err
      else
        $('#txt-name').val('')
        $('#txt-price').val('')
        $('#txt-description').val('')
        $('#txt-name').focus()
        Session.set('imageReset',true)
        Session.set('imageId',undefined)
        $('#btn-update').hide()
        $('#btn-create').show()
        swal({
          title: "Success"
          text: "Item updated successfully"
          icon: "success"
        }).then((ok)-> $('#myModal').modal('hide'))
    $("#btn-update").prop('disabled', false)