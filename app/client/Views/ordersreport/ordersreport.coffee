﻿Template.ordersreport.onCreated () ->
  t = @
  t.list = new ReactiveVar([]) 
  @subscribe 'orders'
  @subscribe 'users'

Template.ordersreport.onRendered () ->
  $('.datepickerTo').datepicker({ format: 'mm-dd-yyyy' })
  $('.datepickerFrom').datepicker({ format: 'mm-dd-yyyy' })
Template.ordersreport.helpers
  getorders: () ->
    t = Template.instance()
    orders = t.list.get()
    
    return orders
  orderedtime: (time) ->
    return moment(time).format('MMMM Do YYYY, h:mm a')
  completed: (status) ->
    return status == "completed"
  canceled: (status) ->
    return status == "canceled"
  pending: (status) ->
    return status == "pending"
 

Template.ordersreport.events
  'click #btn-search':(e,t) ->
    e.preventDefault()
    e.stopPropagation()
    if  $('.datepickerFrom').val()== '' or $('.datepickerTo').val() == '' 
      Bert.alert('Please, choose date','warning')
      return
    start = new Date($('.datepickerFrom').val())
    end = new Date($('.datepickerTo').val())
    start = start.toISOString()
    end = end.toISOString()
    t.list.set(db.orders.find({ 'time': { $gte: start, $lt: end}}).fetch())
 