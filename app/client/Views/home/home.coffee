﻿Template.home.onCreated () ->
  @subscribe 'categories'
  @subscribe 'items'
  

Template.home.helpers
  categories: () ->
    return db.categories.find()
  items: (id) ->
    return db.items.find('categoryid':id)

Template.home.events
  'click [data-action=choose-item]': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    id = e.currentTarget.value
    selecteditem = db.items.findOne({'_id':id})
    Session.set('selecteditem',selecteditem)
    $('#img-selected').attr("src",selecteditem.img)
    $('#img-selected').attr("alt",selecteditem.name)
    $('#lbl-selected-name').text(selecteditem.name)
    $('#lbl-selected-cat').text(selecteditem.categoryname)
    $('#lbl-selected-price').text(selecteditem.price)
    $('#lbl-selected-des').text(selecteditem.description)
    $('#myModal').modal('show') 

