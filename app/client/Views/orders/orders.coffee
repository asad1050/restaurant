﻿Template.orders.onCreated () ->
  
  @subscribe 'orders'
  @subscribe 'users'

Template.orders.helpers
  getorders: () ->
    orders = db.orders.find({'status':"pending"}).fetch()
    return orders
  orderedtime: (time) ->
    return moment(time).format('MMMM Do YYYY, h:mm a')
 

Template.orders.events
  'click [data-action=cancel-item]': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    
    id = e.currentTarget.value
    $("[data-action=cancel-item]").prop('disabled', true)
    Meteor.call 'orders.cancel', id, (err, res) ->
      if (err)
        swal({
          title: "Error"
          text: "Unable to cancel order"
          icon: "error"
        }) 
        console.log err
      else
        swal({
          title: "Success"
          text: "Order is canceled successfully"
          icon: "success"
        })
    $("[data-action=cancel-item]").prop('disabled', false)

  'click [data-action=complete-item]': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    
    id = e.currentTarget.value
    $("[data-action=complete-item]").prop('disabled', true)
    Meteor.call 'orders.complete', id, (err, res) ->
      if (err)
        swal({
          title: "Error"
          text: "Unable to complete order"
          icon: "error"
        }) 
        console.log err
      else
        swal({
          title: "Success"
          text: "Order is completed successfully"
          icon: "success"
        })
    $("[data-action=complete-item]").prop('disabled', false)
 