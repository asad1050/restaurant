﻿Template.signin.events
  'click #btn-signin':(e,t)->
    e.preventDefault()
    e.stopPropagation()
    email = $('#txt-email-si').val()
    password = $('#txt-password-si').val()
    $("#btn-signin").prop('disabled', true)
    if email == '' or password == ''
      Bert.alert('Please enter email and password','warning')
      return
    if not email.includes("@")
      Bert.alert('Please enter correct email','warning')
      return
    Meteor.loginWithPassword(email,password,(err)->
      if err
        Bert.alert('Wrong email or password','danger')
      else
        $('#btn-modal-closesi').click()
        swal({
          title: "Success"
          text: "Signed In Successfully"
          icon: "success"
        }))
    $("#btn-signin").prop('disabled', false)

  'keyup #txt-password-si':(e,t)->
    if e.which == 13
      e.preventDefault()
      e.stopPropagation()
      $("#btn-signin").prop('disabled', true)
      email = $('#txt-email-si').val()
      password = $('#txt-password-si').val()
      if email == '' or password == ''
        Bert.alert('Please enter email and password','warning')
        return
      if not email.includes("@")
        Bert.alert('Please enter correct email','warning')
        return
      Meteor.loginWithPassword(email,password,(err)->
        if err
          Bert.alert('Wrong email or password','danger')
        else
          $('#btn-modal-closesi').click()
          swal({
          title: "Success"
          text: "Signed In Successfully"
          icon: "success"
          }))
      $("#btn-signin").prop('disabled', false)