﻿Meteor.publish 'orders', () -> 
  unless Roles.userIsInRole(Meteor.userId(), ['admin'])
    throw new Meteor.Error('Not logged in')
  return db.orders.find()

Meteor.publish 'users', () -> 
  unless Roles.userIsInRole(Meteor.userId(), ['admin','user'])
    throw new Meteor.Error('Not logged in')
  return db.users.find()

Meteor.publish 'user', () -> 
  unless Roles.userIsInRole(Meteor.userId(), ['admin','user'])
    throw new Meteor.Error('Not logged in')
  return db.users.findOne({'_id':Meteor.userId()}).cursor

Meteor.publish 'items', () -> 
  return db.items.find()

Meteor.publish 'categories', () -> 
  return db.categories.find()

Meteor.publish 'images', () -> 
  return db.Images.find().cursor