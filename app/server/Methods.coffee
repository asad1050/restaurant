﻿if Meteor.isServer
  Meteor.methods

#Role Methods

    'role.isAdmin':()->
      return Roles.userIsInRole(Meteor.userId(), 'admin')
    'user.create':(name, email, password) ->
      id = Accounts.createUser({
          'email':email
          'username': email
          'password': password
          'profile':{'role':'user','name':name}
      })
      Roles.addUsersToRoles(id, 'user')
      if id
        return true
      else
        return false

#Items Methods

    'items.add': (name, img, price, description, categoryname, categoryid) ->
      if name == '' or img == '' or price == '' or categoryname == '' or categoryid == ''
        throw new Meteor.Error('Blank Inputs')
      item = {
      'name':name
      'img': img
      'price':price
      'description':description
      'categoryname':categoryname
      'categoryid':categoryid
      }
      db.items.insert(item)
    
    'items.update': (id, name, img, price, description, categoryname, categoryid) ->
      if name == '' or img == '' or price == '' or categoryname == '' or categoryid == ''
        throw new Meteor.Error('Blank Inputs')
      item = {
      'name':name
      'img': img
      'price':price
      'description':description
      'categoryname':categoryname
      'categoryid':categoryid
      }
      db.items.update(id,item)            


    'items.remove': (id) ->
      if id == ''
        throw new Meteor.Error('Blank Id')
      db.items.remove({'_id':id})


#Categories Methods

    'categories.add': (name, img) ->
      if name == '' or img == ''
        throw new Meteor.Error('Blank Inputs')
      category = {
      'name':name
      'img': img
      }
      db.categories.insert(category)

    'categories.remove': (id) ->
      if id == ''
        throw new Meteor.Error('Blank Id')
      db.categories.remove({'_id':id})

#Orders Methods

    'orders.add': (items, userid, username, time ) ->
      if items == [] or time == '' or userid == ''
        throw new Meteor.Error('Blank Inputs')
      order = {
      'items':items
      'status': "pending"
      'userid':userid
      'username':username
      'time':time.toISOString()
      }
      db.orders.insert(order)

    'orders.cancel': (id) ->
      if id == ''
        throw new Meteor.Error('Blank Id')
      db.orders.update(id, { $set:{'status':"canceled"}})

    'orders.complete': (id) ->
      if id == ''
        throw new Meteor.Error('Blank Id')
      db.orders.update(id, { $set:{'status':"completed"}})
    